package com.stephengware.java.games.chess.bot;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import com.stephengware.java.games.chess.bot.Bot;
import com.stephengware.java.games.chess.state.Bishop;
import com.stephengware.java.games.chess.state.King;
import com.stephengware.java.games.chess.state.Knight;
import com.stephengware.java.games.chess.state.Pawn;
import com.stephengware.java.games.chess.state.Piece;
import com.stephengware.java.games.chess.state.Player;
import com.stephengware.java.games.chess.state.Queen;
import com.stephengware.java.games.chess.state.Rook;
import com.stephengware.java.games.chess.state.State;

/**
 * A chess bot which selects its next move at random.
 * 
 * @author Stephen G. Ware
 */
public class MyBot extends Bot {

	/** A random number generator */
	private final Random random;
	
	/**
	 * Constructs a new chess bot named "usernamehere" and whose random  number
	 * generator (see {@link java.util.Random}) begins with a seed of 0.
	 */
	public MyBot() {
		super("usernamehere");
		this.random = new Random(0);
	}
	
	public int evalFunct(State state) {
		int materialScore = 0;
		for(Piece piece : state.board) {
			if(piece.getClass() == Pawn.class) {
				if(piece.player == Player.WHITE) {
					materialScore+ = materialScore + 1;
				} else {
					materialScore = materialScore - 1;
				}
			}
			else if(piece.getClass() == Rook.class) {
				if(piece.player == Player.WHITE) {
					materialScore = materialScore + 2;
				} else {
					materialScore = materialScore - 2;
				}
			}
			else if(piece.getClass() == Bishop.class) {
				if(piece.player == Player.WHITE) {
					materialScore = materialScore + 2;
				} else {
					materialScore = materialScore - 2;
				}
			}
			else if(piece.getClass() == Knight.class) {
				if(piece.player == Player.WHITE) {
					materialScore = materialScore + 2;
				} else {
					materialScore = materialScore - 2;
				}
			}
			else if(piece.getClass() == Queen.class) {
				if(piece.player == Player.WHITE) {
					materialScore = materialScore + 3;
				} else {
					materialScore = materialScore - 3;
				}
			}
			else if(piece.getClass() == King.class) {
				if(piece.player == Player.WHITE) {
					materialScore = materialScore + 99999;
				} else {
					materialScore = materialScore - 99999;
				}
			}
		}
		return materialScore;
	}

	@Override
	protected State chooseMove(State root) {
		// This list will hold all the children nodes of the root.
		ArrayList<State> children = new ArrayList<>();
		// Generate all the children nodes of the root (that is, all the
		// possible next states of the game.  Make sure that we do not exceed
		// the number of GameTree nodes that we are allowed to generate.
		Iterator<State> iterator = root.next().iterator();
		while(!root.searchLimitReached() && iterator.hasNext())
			children.add(iterator.next());
		// Choose one of the children at random.
		return children.get(random.nextInt(children.size()));
	}
}
