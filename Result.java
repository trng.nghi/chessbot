package com.stephengware.java.games.chess.bot;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import com.stephengware.java.games.chess.bot.Bot;
import com.stephengware.java.games.chess.state.State;
import com.stephengware.java.games.chess.state.Player;
import com.stephengware.java.games.chess.state.Bishop;
import com.stephengware.java.games.chess.state.King;
import com.stephengware.java.games.chess.state.Knight;
import com.stephengware.java.games.chess.state.Pawn;
import com.stephengware.java.games.chess.state.Piece;
import com.stephengware.java.games.chess.state.Queen;
import com.stephengware.java.games.chess.state.Rook;

/**
 * A chess bot which selects its next move at random.
 * 
 * @author Stephen G. Ware
 */
public class MyBot extends Bot {

	/** A random number generator */
	private final Random random;
	private Player player;
	/**
	 * Constructs a new chess bot named "nbtruong" and whose random number
	 * generator (see {@link java.util.Random}) begins with a seed of 0.
	 */
	public MyBot() {
		super("nbtruong");
		this.random = new Random(0);
	}

	
	@Override
	protected State chooseMove(State root) {
		State newRoot = minMax(root);
		System.out.println(newRoot.searchLimitReached());
		return newRoot;
	}
	
	public State minMax(State s) {
		// This list will hold all the children nodes of the root.
		ArrayList<State> children = new ArrayList<>();
		// Generate all the children nodes of the root (that is, all the
		// possible next states of the game. Make sure that we do not exceed
		// the number of GameTree nodes that we are allowed to generate.
		Iterator<State> iterator = s.next().iterator();
		Result best = new Result(s, 0);
		
		while (!s.searchLimitReached() && iterator.hasNext())
			children.add(iterator.next());
	
		// Choose one of the children at random.
		if (s.player == Player.WHITE) {
				best = findMax(s, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 3);
		} else {
				best = findMin(s, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 3);
		}
		State bestState = best.s;
		while(bestState.previous != s) {
			bestState = bestState.previous;
		}
		return bestState;
	}

	/**
	 * @param alpha the highest utility value discovered so far in this branch of
	 *              the tree (i.e. best for X)
	 * @param beta  the lowest utility value discovered so far in this branch of the
	 *              tree (i.e. best for O)
	 * @return the utility value of the node with the highest minimum utility
	 */
	private Result findMax(State s, double alpha, double beta, double depthLimit) {
		if (s.over || depthLimit == 0) {
			Result newResult = new Result(s, evaluate(s));
			return newResult;
		}
		ArrayList<State> children = new ArrayList<>();
		Iterator<State> iterator = s.next().iterator();
		double max = Double.NEGATIVE_INFINITY;
		Result currentBest = new Result(s, 0);
		while (!s.searchLimitReached() && iterator.hasNext()) {
			children.add(iterator.next());
		}
		for(State child : children) {
			Result value = findMin(child, alpha, beta, depthLimit-1);
			if (value.evaluate > max) {
				max = value.evaluate ;
				currentBest = value;
			}
			
			if (beta <= value.evaluate) {
				return currentBest;
			}
			if (max > alpha) {
				alpha = Math.max(alpha, max);
			}
		}
		

		return currentBest;
	}

	/**
	 * @param alpha the highest utility value discovered so far in this branch of
	 *              the tree (i.e. best for X)
	 * @param beta  the lowest utility value discovered so far in this branch of the
	 *              tree (i.e. best for O)
	 * @return the utility value of the node with the lowest maximum utility
	 */
	private Result findMin(State s, double alpha, double beta, double depthLimit) {
		// This method is simply the opposite of #findMax.
		if (s.over || depthLimit == 0) {
			Result newResult = new Result(s, evaluate(s));
			return newResult;
		}
		ArrayList<State> children = new ArrayList<>();
		Iterator<State> iterator = s.next().iterator();
		double min = Double.POSITIVE_INFINITY;
		Result currentBest = new Result(s, 0);
		while (!s.searchLimitReached() && iterator.hasNext()) {
			children.add(iterator.next());
		}
		for(State child : children) {
			Result value = findMax(child, alpha, beta, depthLimit-1);
			if (value.evaluate < min) {
				min = value.evaluate ;
				currentBest = value;
			}
			
			if (value.evaluate <= alpha) {
				return currentBest;
			}
			if (min < beta) {
				beta = Math.min(beta, min);
			}
		}
		return currentBest;
	}

	private double evaluate(State s) {
		if (s.over) {
			if (s.check) {
				if (s.player == Player.WHITE) {
					return -99990;
				} else {
					return 99990;
				}
			}
		}
		
		double score = 0.0;

		for (Piece p : s.board) {
			if (p.player == Player.WHITE) {
				if (p.getClass() == Pawn.class) {
					score += 1.0;
				}
			} else {
				if (p.getClass() == Pawn.class) {
					score -= 1.0;
				}
			}
			if (p.player == Player.WHITE) {
				if (p.getClass() == Knight.class) {
					score += 2.0;
				}
			} else {
				if (p.getClass() == Knight.class) {
					score -= 2.0;
				}
			}

			if (p.player == Player.WHITE) {
				if (p.getClass() == Bishop.class) {
					score += 3.0;
				}
			} else {
				if (p.getClass() == Bishop.class) {
					score -= 3.0;
				}
			}
			
			if (p.player == Player.WHITE) {
				if (p.getClass() == Rook.class) {
					score += 5.0;
				}
			} else {
				if (p.getClass() == Rook.class) {
					score -= 5.0;
				}
			}

			if (p.player == Player.WHITE) {
				if (p.getClass() == Queen.class) {
					score += 7.0;
				}
			} else {
				if (p.getClass() == Queen.class) {
					score -= 7.0;
				}
			}
			if (p.player == Player.WHITE) {
				if (p.getClass() == King.class) {
					score += 99.0;
				}
			} else {
				if (p.getClass() == King.class) {
					score -= 99.0;
				}
			}
		}
		return score;
	}
	
	public class Result{
		public State s;
		public double evaluate;

		public Result(State s, double evaluate) {
			this.s = s;
			this.evaluate = evaluate;
		}
	}
}
